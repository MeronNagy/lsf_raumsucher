Rails.application.routes.draw do
  resources :room_events, only: [:index, :create, :show, :update, :edit, :destroy]
  get 'pages/about', as: "about"
  resources :rooms, only: [:index, :show] do
    resources :room_events, only: [:new]
  end
  #post '/room_events', to: redirect('/rooms/:id/room_events')
  root to: redirect('/rooms?locale=en')
  
  devise_for :users, :controllers => { :registrations => "registrations_mail" }
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
