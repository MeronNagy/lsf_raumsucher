require 'test_helper'

class RoomEventTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  def setup
    @room_event = RoomEvent.new(room_id: 1, user_id: 1, frequency: "Once", interval: "0" ,end: "20:55", start: "21:30", description: "test")
  end
  
  test "room_event should be valid" do
    assert @room_event.valid?
  end
  
  test "room_id should be present" do
    @room_event.room_id =""
    assert_not @room_event.valid?
  end
  
  test "user_id should be present" do
    #puts @room_event.room_id
    @room_event.user_id =""
    assert_not @room_event.valid?
  end

  test "description should be present" do
    @room_event.description =""
    assert_not @room_event.valid?
  end

  test "start should be present" do
    @room_event.start =""
    assert_not @room_event.valid?
  end

  test "end should be present" do
    @room_event.end =""
    assert_not @room_event.valid?
  end
  
  test "interval should be present" do
    @room_event.interval =""
    assert_not @room_event.valid?
  end

  test "frequency should be present" do
    @room_event.frequency =""
    assert_not @room_event.valid?
  end

  test "start should be HH:MM" do
    @room_event.start = "test"
    assert_not @room_event.valid?
  end
   
  test "end should be HH:MM" do
    @room_event.end = "test"
    assert_not @room_event.valid?
  end

  test "room_id should be numeric" do
    @room_event.room_id = "test"
    assert_not @room_event.valid?
  end

  test "user_id should be numeric" do
    @room_event.user_id = "test"
    assert_not @room_event.valid?
  end
  
  test "room_id should be positive integer" do
    @room_event.room_id = -5
    assert_not @room_event.valid?
  end

  test "user_id should be positive integer" do
    @room_event.user_id = -5
    assert_not @room_event.valid?
  end

  test "interval should be numeric" do
    @room_event.user_id = "test"
    assert_not @room_event.valid?
  end
 
  test "interval should be positive integer" do
    @room_event.room_id = -5
    assert_not @room_event.valid?
  end
  test "frequency should include Once, DAILY or WEEKLY" do
    @room_event.frequency = "Test"
    assert_not @room_event.valid?
    @room_event.frequency = "Once"
    assert @room_event.valid?
    @room_event.frequency = "DAILY"
    assert @room_event.valid?
    @room_event.frequency = "WEEKLY"
    assert @room_event.valid?


  end
end
