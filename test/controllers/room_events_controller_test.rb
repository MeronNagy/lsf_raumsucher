require 'test_helper'

class RoomEventsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @room_event = room_events(:one)
  end

  test "should get index" do
    get room_events_url
    assert_response :success
  end

  test "should get new" do
    get new_room_room_event_url(1)
    assert_response :redirect
  end

  test "should ask user to sign in before create" do
    
    post room_events_url, params: { room_event: { description: @room_event.description, end: @room_event.end, room_id: @room_event.room_id, start: @room_event.start, user_id: @room_event.user_id } }
    assert_redirected_to '/users/sign_in'
  end

  test "should show room_event" do
    get room_event_url(@room_event)
    assert_response :success
  end

  test "should ask user to sign_in before get edit" do
    get edit_room_event_url(@room_event)
    assert_redirected_to '/users/sign_in'
  end

  test "should ask user to sign_in before update room_event" do
    patch room_event_url(@room_event), params: { room_event: { description: @room_event.description, end: @room_event.end, room_id: @room_event.room_id, start: @room_event.start, user_id: @room_event.user_id } }
    assert_redirected_to '/users/sign_in'
  end

  test "should ask user to sign_ in before destroy room_event" do
    assert_difference('RoomEvent.count', 0) do
      delete room_event_url(@room_event)
    end

    assert_redirected_to '/users/sign_in'
  end
end
