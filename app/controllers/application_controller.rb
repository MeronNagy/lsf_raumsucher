class ApplicationController < ActionController::Base
  def default_url_options(options={})
    { locale: I18n.locale }
  end
  before_action :check_for_locale_change
  before_action :set_locale

def check_for_locale_change
  new_locale = params[:new_locale]

  if new_locale.present? && new_locale != params[:locale]
    redirect_to url_for(params.merge(locale: new_locale, set_locale: nil))
  end
end

def set_locale
  I18n.locale = params[:locale] || session[:locale] || extract_locale_from_accept_language_header || I18n.default_locale
  session[:locale] = I18n.locale
end
  around_action :switch_locale
  def switch_locale(&action)
    locale = params[:locale] || I18n.default_locale
    I18n.with_locale(locale, &action)
  end
  #force_ssl
end
