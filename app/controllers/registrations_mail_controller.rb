class RegistrationsMailController < Devise::RegistrationsController

  def create
    super
    if @user.persisted?
      UserRegistrationMailer.send_signup_email(@user).deliver
    end
  end

end
