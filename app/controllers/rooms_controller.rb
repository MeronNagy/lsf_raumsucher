require 'will_paginate/array'
class RoomsController < ApplicationController
	def index
		@rooms = Room.search(params[:roomtype], params[:campus], params[:day], params[:starttime], params[:endtime]).paginate(:per_page => 20, :page => params[:page])
		@room_events = RoomEvent.all()
	end
	def show
		@room = Room.find(params[:id])
		@room_events = @room.room_events
	end
end
