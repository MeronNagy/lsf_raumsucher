class RoomEventsController < ApplicationController
  before_action :authenticate_user!, only: [:new, :create, :edit, :update, :destroy]
  before_action :set_room_event, only: [:show, :edit, :update, :destroy]
  # GET /room_events
  # GET /room_events.json
  def index
    @room_events = RoomEvent.all
  end

  # GET /room_events/1
  # GET /room_events/1.json
  def show
  end

  # GET /room_events/new
  def new
    @room_event = RoomEvent.new
   end

  # GET /room_events/1/edit
  def edit
  end

  # POST /room_events
  # POST /room_events.json
  def create
    #@room_event = RoomEvent.new(room_event_params.except(:day).except(:special_form))
    parameters =  room_event_params
    if parameters[:start].present? && parameters[:end].present? && parameters[:day].present?
      puts parameters[:start]
      starttime = (parameters[:day].gsub '-', '') + "T" + (parameters[:start].sub ':', '') + "00"
      endtime = (parameters[:day].gsub '-', '') + "T" + (parameters[:end].sub ':', '') + "00"
      parameters[:start] = starttime
      parameters[:end] = endtime
      #@room_event = RoomEvent.new(room_event_params.except(:day).except(:special_form))
    else 
      puts "fail"
      parameters[:start] = "0"
    end
    puts parameters[:start]
    @room_event = RoomEvent.new(parameters.except(:day).except(:locale).except(:special_form))
    respond_to do |format|
      if parameters[:special_form].present? && @room_event.save
        format.html { redirect_to @room_event, notice: 'Room event was successfully created.' }
        format.json { render :show, status: :created, location: @room_event }
      else 
        format.html { render :new }
	if @room_event.nil?
          format.json { render json: { message: "unprocessable_entity"} , status: :unprocessable_entity }
        else
	  format.json { render json: @room_event.errors , status: :unprocessable_entity }      
	end
      end
    end
  end

  # PATCH/PUT /room_events/1
  # PATCH/PUT /room_events/1.json
  def update
    parameters =  room_event_params
    if current_user.id != @room_event.user.id
      parameters[:user_id] = -1
    end
    if parameters[:start].present? && parameters[:end].present? && parameters[:day].present?
      parameters[:start] = (parameters[:day].gsub '-', '') + "T" + (parameters[:start].sub ':', '') + "00"
      parameters[:end] = (parameters[:day].gsub '-', '') + "T" + (parameters[:end].sub ':', '') + "00"
    else
      parameters[:start] = "0"
    end
    respond_to do |format|
      if @room_event.update(parameters.except(:day).except(:special_form))
        format.html { redirect_to @room_event, notice: 'Room event was successfully updated.' }
        format.json { render :show, status: :ok, location: @room_event }
      else
        format.html { render :edit }
        format.json { render json: @room_event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /room_events/1
  # DELETE /room_events/1.json
  def destroy
    if current_user.id == @room_event.user.id
      @room_event.destroy!
      respond_to do |format|
        format.html { redirect_to room_events_url, notice: 'Room event was successfully destroyed.' }
        format.json { head :no_content }
    end

    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_room_event
      @room_event = RoomEvent.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def room_event_params
      params.require(:room_event).permit(:start, :end, :description, :room_id, :user_id, :day, :frequency, :interval, :special_form)
    end
end
