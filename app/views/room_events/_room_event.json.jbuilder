json.extract! room_event, :id, :start, :end, :description, :room_id, :user_id, :created_at, :updated_at
json.url room_event_url(room_event, format: :json)
