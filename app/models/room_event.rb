class RoomEvent < ApplicationRecord
  belongs_to :room
  belongs_to :user
  validates :frequency, inclusion: { in: ["Once", "DAILY", "WEEKLY"] }
  validates :description, length: {maximum: 200}, presence: true
  validates :room_id, :user_id, :interval, numericality: { only_integer: true, :greater_than_or_equal_to => 0}
  validates :start, :end, presence: true
  #, format: {with:/\A\d{2}:\d{2}\z/}
  #format: {with:/\A(\d){4}[01]\d[012]\dT[012]\d([0-6]\d){2}\z/}

end
