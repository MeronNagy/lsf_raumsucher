class UserRegistrationMailer < ApplicationMailer
  default :from => 'admin@lsfraumsucher.com'

  # send a signup email to the user, pass in the user object that   contains the user's email address
  def send_signup_email(user)
    @user = user
    if !@user.nil?
      mail( :to => @user.email,
      :subject => 'Thanks for signing up for LSF-Raumsucher' )
    end
  end
end
