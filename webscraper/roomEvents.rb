require 'pg' 
def getData (line)
        data = false
        text = ""
        line.split("").each do |i|
                if data == true
                        text = text + i
                        next
                end
                if i == ":"
                        data = true
                end
        end
        return text.gsub("\n","").gsub(/<.*?>/,"")
end

def addRoomEventToDB(id, roomid, dtstart, dtend, description, frequency, interval)
    con = PG.connect :dbname => 'lsfraumsucher_development',
        :user => 'lsfraumsucher',
        :password => 'password'

    userid = 0 #Admin
    sqlquery = 	"INSERT INTO room_events VALUES (" + id + 
	",'" + dtstart + "','" + dtend + "','" + description + 
	"'," + roomid + "," + userid.to_s + 
	",'" + Time.now.getutc.to_s + "','" + Time.now.getutc.to_s+ 
	"','" + frequency + "'," + interval +")"
    con.exec sqlquery   
rescue PG::Error => e
    puts "pg error:"
    puts e.message
ensure
    con.close if con
end


def readiCal(ical, i, j)
	foundBegin = false
	dtstart = "";
	dtend = "";
	frequency = "Once";
	interval = "0";
	description = "";
	roomid = i.to_s
	ical.each_line do |line| 
		if line.include? "END:VEVENT"
			#$b.execute("INSERT INTO ICAL (DTSTART, DTEND, LOCATION, DTSTAMP, UID, DESCRIPTION, SUMMARY, CATEGORIES)
			#VALUES (?, ?, ?, ?, ?, ?, ?, ?)", [dtstart, dtend, location, dtstamp, Integer(uid), description, summary, categories])
        	       	puts j.to_s + ", " + roomid + ", " + dtstart + ", " + dtend + ", " + description
			addRoomEventToDB(j.to_s, roomid, dtstart, dtend, description, frequency, interval)
			foundBegin = false
			j = j-1
			next
        	end
		if foundBegin == true
			if line.include? "DTSTART;"
				dtstart = getData(line)
				next
			end
			if line.include? "DTEND;"
                        	dtend = getData(line)
				next
			end
			if (line.include? "FREQ=") || (line.include? "BYDAY=")
				if line.include? "BYDAY=MO,TU,WE,TH,FR"
					frequency = "DAILY"
				else
					temp = line.slice(line.index("FREQ=")..line.length)
        	                        frequency = temp.slice("FREQ=".length..temp.index(";")-1)
	
				end
			end
			if line.include? "INTERVAL="
				temp = line.slice(line.index("INTERVAL=")..line.length)
				if temp.include? ";"
					interval = temp.slice("INTERVAL=".length..temp.index(";")-1)
				else
					interval = temp.slice("INTERVAL=".length..temp.length)
				end
			end					
			if line.include? "DESCRIPTION:"
				description = getData(line)
				next
			end
			if (line.include? "SUMMARY:") && (description.eql? "")
				description = getData(line)
				next
			end
		end
	        if line.include? "BEGIN:VEVENT"
	                foundBegin = true
	        end     
	end
	return j
end

i = 0
j = 2**(8*4)/2 -1
loop do
	if i >= 10000 
		break
	end
	name = "setup/ical/raum" + i.to_s + ".ical"
	if File.file?(name) == true
		ical = open(name)
		j = readiCal(ical, i, j)
	end	
	i+=1
end
