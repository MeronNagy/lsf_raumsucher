#!/usr/bin/ruby
# setup.rb
require 'open-uri'
require 'nokogiri'
require 'csv'
require 'pg'
n = 0
num = 10000
#Prüft ob "setup"  Ordner vorhanden ist wenn nein erstellt er diesen
if File.directory?("setup")
	puts "setup directory found"
else
	Dir.mkdir("setup")
end
if File.directory?("setup/ical")
	puts "setup/ical directory found"
else
	Dir.mkdir("setup/ical")
end
#Prüft ob notroom Datei existiert wenn nein wird diese erstellt
if File.exist?("setup/notroom")
	puts "notroom file found"
else
	File.open("setup/notroom", "w")
end

if File.exist?("setup/rooms.csv")
	puts "rooms.csv file found"
else
        File.open("setup/rooms.csv", "w")
end

begin	
	if n%1000==0
                puts (100*n/10000).to_s + " %"
        end
	if File.readlines("setup/notroom").grep(/^#{n}$/).size > 0
		#puts n.to_s + " is not a room (skipped)"
		n+=1
		next
	end
	if n < 10
		url = 'https://lsf.htw-berlin.de/qisserver/rds?state=wplan&act=Raum&pool=Raum&raum.rgid=000' +n.to_s
	elsif n < 100
		url = 'https://lsf.htw-berlin.de/qisserver/rds?state=wplan&act=Raum&pool=Raum&raum.rgid=00'+n.to_s
	elsif n < 1000
		url = 'https://lsf.htw-berlin.de/qisserver/rds?state=wplan&act=Raum&pool=Raum&raum.rgid=0'+n.to_s
	else
		url = 'https://lsf.htw-berlin.de/qisserver/rds?state=wplan&act=Raum&pool=Raum&raum.rgid='+n.to_s
	end
	html = open(url)
	doc = Nokogiri::HTML(html)	
	txt = doc.inner_html	
	boolCal = false
	boolNotRoom = false
        txt.each_line do |line|
		#Prüft ob Raum existiert
		if line.include? "Ihre Eingaben haben zu keinem Ergebnis"
                        open('setup/notroom', 'a') { |f|
                                f.puts n
                        }
                        #puts "Not a room " + n.to_s
                        boolNotRoom = true
                        n+=1
                        next
                end

		#Prüft Raumart
        	if line.include? "\u2022".force_encoding("ISO-8859-1")
			roomid = (n.to_s) +";"
			type = ""
			#Raum bereits in rooms.csv eingetragen
			if File.open('setup/rooms.csv').each_line.any?{|li| li.include?roomid}
                		#puts "Room known"
			#Raum nicht in der rooms.csv aufgelistet
			else
				#Die Zeile mit der Raumart enthält einen Bulletpoint \u2022 
				#Nach dem Bulletpoint findet man eine lange Raumbeschreibung
				#Und eine abgekürzte Raumbeschreibung in Klammern (Raumart)
				#In Einigen Fällen findet man jedoch nur eine der beiden Beschreibungen
				
				#Wenn lange Raumbeschreibung vorhanden ist
				if line.index(" (") - line.index("\u2022".force_encoding("ISO-8859-1"))	> 4		
	                        	if (line.count"(") >= 3
						type = line.slice(line.index("\u2022".force_encoding("ISO-8859-1"))+4..line.index(') ')+1)	
				else
						type = line.slice(line.index("\u2022".force_encoding("ISO-8859-1"))+4..line.index(" (")-1)
						type2 = line.slice(line.index(' (')+2..line.rindex(')')-1)
						#puts type
						#puts type2
						if (type.length < type2.length) && !(type2.include?".")
							type = type2
						end 
					end
					#Wenn nur abgekürzte Raumbeschreibung vorhanden ist
				else
					type = line.slice(line.rindex(' (')+2..line.rindex(')')-1)
				end
				#puts type
				#Prüft ob Raum zum Campus TA, WH, TGS oder PB gehört
				if line.include? "TA" or line.include? "WH" or line.include? "TGS" or line.include? "PB"  
					#Sonderfall TGS wird ab und zu als "WH TGS" bezeichnet"
					if line.include? "WH TGS"
						line.sub! 'WH ', ''
					end
					#Sonderfall TGS Haus x
					if line.include? "Haus"
						line.sub! 'Haus ', ''
					end
					type.sub! '&amp;', '&'
					str = line[0, line.index('(')]
					str.sub! 'Gebäude'.force_encoding("ISO-8859-1"), ''
					values = str.split()
					#puts str
					campus = values[0]
					building = values[1]
					room = values[2]
					open('setup/rooms.csv', 'a') { |f|
						f.puts n.to_s + ";" + type + ";" + room + ";" + 
						building + ";" + campus + ";" + 
						Time.now.getutc.to_s + ";" + Time.now.getutc.to_s
					}
				end
			end
		end
		#Prüft ob iCalendar Datei vorhanden
		if line.include? "moduleCall=iCalendar"
                        url = line
                        boolCal = true
                        break
                end

	end
	#Keine ical Datei = Raum ist die ganze Woche frei
	if boolCal == false
		#puts "Available " + n.to_s
		n+=1
		next
	#Ical Datei vorhanden
	else 
		#puts "Is a room " + n.to_s
	end
	#url anpassen
	for i in 0...url.length
        	if url[i].chr == '"'
                	url = url[i+1..url.length]
                	break
        	end
	end
	for i in 0...url.length
        	if url[i].chr == '"'
                	url = url[0..i-1]
                	break
        	end
	end
	#Download ical
	if url != ""
		url = url.gsub(/&amp;/, "&")
		url = url.gsub(/;jsession.*qis5/, "")
		#puts "ical " + n.to_s
		download = open(url)
		IO.copy_stream(download, 'setup/ical/raum' + n.to_s + '.ical')
	end
	n += 1
	#sleep(2)	
end until n >= num

begin
    con = PG.connect :dbname => 'lsfraumsucher_development',
        :user => 'lsfraumsucher',
        :password => 'password'

    user = con.user
    db_name = con.db
    pswd = con.pass

    puts "User: #{user}"
    puts "Database name: #{db_name}"
    puts "Password: #{pswd}"
    con.exec "
	INSERT INTO users (id, email, encrypted_password, created_at, updated_at) 
	VALUES (0, 'admin@lsfraumsucher.htw', 'password', current_timestamp, current_timestamp) 
	ON CONFLICT DO NOTHING;
	"
    con.exec "
        COPY rooms (id, roomtype, roomnr, building, campus, created_at, updated_at)
        FROM '/home/user/lsfraumsucher/webscraper/setup/rooms.csv'
        CSV DELIMITER ';';
        "
rescue PG::Error => e
    puts "pg error:"
    puts e.message
ensure
    con.close if con
end

