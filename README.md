
# LSF Raumsucher
https://lsf-raumsucher.herokuapp.com
Projekt für Webentwicklung SoSe2019

## Idee

Studenten wollen sich zum gemeinsamen Lernen an der Hochschule treffen. 

### Probleme
* Die Mensa ist zu laut 
* in der Bibliothek kann man nicht diskutieren
* Man braucht eventuell PCs oder ein Labor

### Lösung 
LSF Raumsucher der alle verfügbaren Räume an der HTW auflistet.

### Lösungsansatz
* Webcrawler der einen Raumplan erstellt und die Veranstaltungen einträgt.
* Webseite an der sich Studenten registrieren können, Räume suchen und diese als belegt Kennzeichnen können.

# Features
* Webcrawler der sich die Raum- und Stundenpläne der HTW runterlädt und in die postgres Datenbank einträgt.
* Suchen von Freien Räumen
* Filter für Campus, Gebäude, Raumart (Laborraum, Vorlesungsraum, usw.)
* Sprachen Deutsch und Englisch
* User Registration 
* "Buchen" von freien Räumen durch User
