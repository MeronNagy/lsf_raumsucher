class AddFrequencyToRoomEvents < ActiveRecord::Migration[6.0]
  def change
    add_column :room_events, :frequency, :string
  end
end
