class CreateRoomEvents < ActiveRecord::Migration[6.0]
  def change
    create_table :room_events do |t|
      t.string :start
      t.string :end
      t.string :description
      t.references :room, null: false, foreign_key: true
      t.belongs_to :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end
