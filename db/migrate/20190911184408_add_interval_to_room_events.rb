class AddIntervalToRoomEvents < ActiveRecord::Migration[6.0]
  def change
    add_column :room_events, :interval, :integer
  end
end
