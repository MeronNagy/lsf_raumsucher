class CreateRooms < ActiveRecord::Migration[6.0]
  def change
    create_table :rooms do |t|
      t.string :type
      t.string :roomnr
      t.string :building
      t.string :campus

      t.timestamps
    end
  end
end
